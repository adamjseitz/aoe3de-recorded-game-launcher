#!/usr/bin/env python3
import argparse
import fnmatch
import hashlib
import json
import os
import shutil
import sys
import zipfile

import pefile
from aoefile import alz4
from aoefile import barfile

DIFF_EXCLUDE_PATHS = [
    'AoE3DE_s.exe',
    'Game\\AI\\*', 
    'Game\\Art\\ui\\fonts\\aomgeneratefontmask.exe',
    'Game\\Campaign\\*', 
    'Game\\Data\\streamingdata\\blood ice steel\\*.texprimer',
    'Game\\Data\\streamingdata\\TextureCache.cache',
    'Game\\Data\\strings\\*',
    'Game\\Movies\\*',
    'Game\\RandMaps\\*', 
    'Game\\Render\\*',
    'Game\\Sound\\*',
    'Game\\UI\\*',
    'Startup\\*',
    'install.vdf',
    '*.ddt',
    '*.psd',
    'live.dat',
]

EXCLUDE_MANIFESTS = [
    '100_12_1529_0_Release',  # For the purpose of recs, the initial release is the same as the day-1 hotfix
]

REC_VERSION_MAP = {
    '100.12.1529.0': '132601',
    '100.12.3552.0': '134624',
    '100.12.4087.0': '135159',
    '100.12.5025.0': '136097',
    '100.12.5208.0': '136280',
    '100.12.6159.0': '137231',
    '100.12.6847.0': '137919',
}

#ARCHIVE_PATH = '/mnt/c/Users/Adam/Desktop/Betas/AoE3DE/Archive'
ARCHIVE_PATH = 'C:/Users/Adam/Desktop/Betas/AoE3DE/Archive'
ARCHIVE_FILES_DIR = 'Files'
ARCHIVE_MANIFESTS_DIR = 'Manifests'

def expand_manifest_path(path):
    if not path.endswith('.json'):
        path += '.json'
    if os.path.dirname(path) == '':
        path = os.path.join(ARCHIVE_PATH, ARCHIVE_MANIFESTS_DIR, path)

    return path

def write_manifest(path, content):
    path = expand_manifest_path(path)
    with open(path, 'w') as manifest_file:
        json.dump(content, manifest_file)

def read_manifest(path):
    path = expand_manifest_path(path)
    with open(path, 'r') as manifest_file:
        content = json.load(manifest_file)
    return content 

def hash_data(data):
    hasher = hashlib.sha256()
    hasher.update(data)
    return hasher.hexdigest()

def hash_file(file_path):
    # Don't want new process overhead on small files
    hasher = hashlib.sha256()
    with open(file_path, 'rb') as file_to_hash:
        while 1:
            data = file_to_hash.read(4096)
            if not data:
                break
            hasher.update(data)
    return hasher.hexdigest()

def rebuild_bar(target_path, metadata):
    """
    Rebuild a BAR file from its metadata
    """
    os.makedirs(os.path.dirname(target_path), exist_ok=True)
    with open(target_path, 'wb') as bar_file_handle:
        hdr_path = os.path.join(ARCHIVE_PATH, ARCHIVE_FILES_DIR, metadata['header'])
        with open(hdr_path, 'rb') as hdr_handle:
            shutil.copyfileobj(hdr_handle, bar_file_handle)

        for file_meta in metadata['entries']:
            archived_path = os.path.join(ARCHIVE_PATH, ARCHIVE_FILES_DIR, file_meta['h'])
            with open(archived_path, 'rb') as entry_handle:
                bar_file_handle.seek(file_meta['o'], os.SEEK_SET)
                shutil.copyfileobj(entry_handle, bar_file_handle)

        table_path = os.path.join(ARCHIVE_PATH, ARCHIVE_FILES_DIR, metadata['file_table'])
        bar_file_handle.seek(metadata['file_table_offset'], os.SEEK_SET)
        with open(table_path, 'rb') as table_handle:
            shutil.copyfileobj(table_handle, bar_file_handle)

def process_bar(target_archive):
    """
    Archive a BAR file and generate metadata
    """
    tmpdir = os.path.join(ARCHIVE_PATH, 'tmp')
    os.makedirs(tmpdir, exist_ok=True)

    bar_reader = barfile.BARReader.from_bar(target_archive)
    
    table_offset = bar_reader.source.filetable_offset 
    first_entry_offset = min(e.source.file_offset for e in bar_reader.files)

    metadata = {}

    with open(bar_reader.source.file_path, 'rb') as bar_file_handle:
        bar_file_handle.seek(0, os.SEEK_SET)
        header_data = bar_file_handle.read(first_entry_offset)
        hash_str = hash_data(header_data)
        metadata['header'] = hash_str

        with open(os.path.join(ARCHIVE_PATH, ARCHIVE_FILES_DIR, hash_str), 'wb') as header_file:
            header_file.write(header_data)

        bar_file_handle.seek(table_offset, os.SEEK_SET)
        table_data = bar_file_handle.read()
        hash_str = hash_data(table_data)

        with open(os.path.join(ARCHIVE_PATH, ARCHIVE_FILES_DIR, hash_str), 'wb') as table_file:
            table_file.write(table_data)

        metadata['file_table_offset'] = table_offset
        metadata['file_table'] = hash_str

    metadata['entries'] = []
    for entry in bar_reader.files:
        extracted_file = entry.extract(tmpdir, flatten=True, decompress=False)
        hash_str = hash_file(extracted_file)
        metadata['entries'].append({'p': entry.file_name, 'h': hash_str, 'o': entry.source.file_offset})

        target_path = os.path.join(ARCHIVE_PATH, ARCHIVE_FILES_DIR, hash_str)

        if not os.path.exists(target_path):
            os.rename(extracted_file, target_path)

    shutil.rmtree(tmpdir)
    return metadata

def get_version(root):
    executable = pefile.PE(os.path.join(root, 'AoE3DE_s.exe'))
    return executable.FileInfo[0][0].StringTable[0].entries[b'FileVersion'].decode()

def cmd_archive(args):
    manifest = {}
    manifest['version'] = get_version(args.root)
    manifest['files'] = []

    for (dirpath, _, filenames) in os.walk(args.root):
        for filename in filenames:
            filepath = os.path.join(dirpath, filename)
            rel_filepath = os.path.relpath(filepath, start=args.root)
            print(rel_filepath, end=' ')
            sys.stdout.flush()

            src = os.path.join(args.root, rel_filepath)
            hash_str = hash_file(filepath)
            ext = os.path.splitext(filename)[1]
            if ext.lower() == '.bar':
                bar_metadata = process_bar(src)
                bar_metadata['type'] = 'bar'
                bar_metadata['h'] = hash_str
                bar_metadata['p'] = rel_filepath
                manifest['files'].append(bar_metadata)
            else:
                dst = os.path.join(ARCHIVE_PATH, ARCHIVE_FILES_DIR, hash_str)
                print('{} -> {}'.format(src, dst))
                shutil.copyfile(src, dst)
                manifest['files'].append({'h': hash_str, 'p': rel_filepath})

    write_manifest(args.version, manifest)

def cmd_rebuild(args):
    if os.path.exists(args.root):
        print('Directory already exists!')
        sys.exit(1)

    os.mkdir(args.root)

    files_dir = os.path.join(ARCHIVE_PATH, ARCHIVE_FILES_DIR)
    manifest_path = os.path.join(ARCHIVE_PATH, ARCHIVE_MANIFESTS_DIR, args.version + '.json')
    manifest = read_manifest(manifest_path)

    for file_meta in manifest['files']:
        path = file_meta['p']
        hash_str = file_meta['h']

        dst = os.path.join(args.root, path)

        if 'type' in file_meta and file_meta['type'] == 'bar':
            print('BAR -> {}'.format(dst))
            rebuild_bar(dst, file_meta)

        else:
            src = os.path.join(files_dir, hash_str)
            print('{} -> {}'.format(src, dst))

            os.makedirs(os.path.dirname(dst), exist_ok=True)
            shutil.copyfile(src, dst)

def cmd_diff(args):
    targets = fnmatch.filter(os.listdir(os.path.join(ARCHIVE_PATH, ARCHIVE_MANIFESTS_DIR)), '*.json')

    base_manifest = read_manifest(args.base)

    cache_path = os.path.join(args.output, 'Files')
    os.makedirs(cache_path, exist_ok=True)

    for target_filename in targets:
        target = os.path.splitext(target_filename)[0]
        if target in EXCLUDE_MANIFESTS:
            continue

        target_manifest = read_manifest(target_filename)

        diff_manifest = {}
        diff_manifest['target'] = target_manifest['version']
        diff_manifest['base'] = base_manifest['version']
        try:
            diff_manifest['recid'] = REC_VERSION_MAP[target_manifest['version']]
        except KeyError:
            diff_manifest['recid'] = ''

        diff_manifest['files'] = []
        diff_manifest['restore_files'] = []

        base_files_by_path = {}
        for bf in base_manifest['files']:
            base_files_by_path[bf['p']] = bf

        package_files = diff_manifest['files']
        restore_files = diff_manifest['restore_files']

        for tf in target_manifest['files']:
            if any(fnmatch.fnmatch(tf['p'], expath) for expath in DIFF_EXCLUDE_PATHS):
                continue

            try:
                bf = base_files_by_path[tf['p']]
            except KeyError:
                print('Need:', tf['p'])
                package_files.append(tf)
                continue

            if tf['h'] == bf['h']:
                continue

            print('Diff:', tf['p'])
            if not ('type' in tf and tf['type'] == 'bar'):
                package_files.append(tf)
                restore_files.append(bf)
                continue

            bar_base_files_by_path = {}
            for bf_entry in bf['entries']:
                bar_base_files_by_path[bf_entry['p']] = bf_entry

            for tf_entry in tf['entries']:
                entry_game_path = os.path.join(os.path.dirname(tf['p']), tf_entry['p'])
                if any(fnmatch.fnmatch(entry_game_path, expath) for expath in DIFF_EXCLUDE_PATHS):
                    continue
                try:
                    bf_entry = bar_base_files_by_path[tf_entry['p']]
                except KeyError:
                    print('  Need:', tf_entry['p'])
                    tf_entry['p'] = os.path.relpath(entry_game_path, 'Game')
                    del tf_entry['o']
                    package_files.append(tf_entry)
                    continue

                if tf_entry['h'] != bf_entry['h']:
                    print('  Diff:', tf_entry['p'])
                    tf_entry['p'] = os.path.relpath(entry_game_path, 'Game')
                    del tf_entry['o']
                    package_files.append(tf_entry)
                    
        for file_info in package_files + restore_files:
            src_path = os.path.join(ARCHIVE_PATH, ARCHIVE_FILES_DIR, file_info['h'])
            dst_path = os.path.join(cache_path, file_info['h'])
            shutil.copyfile(src_path, dst_path)

        with open(os.path.join(args.output, target + '.json'), 'w') as manifest_file:
            json.dump(diff_manifest, manifest_file)

def main():
    parser = argparse.ArgumentParser(description='Add a version to an Archive')
    subparsers = parser.add_subparsers()
    archive_cmd = subparsers.add_parser('archive', help='Add a new patch to the archive')
    archive_cmd.add_argument('version', help='Version name')
    archive_cmd.add_argument('root', help='Root Directory')
    archive_cmd.set_defaults(func=cmd_archive)

    rebuild_cmd = subparsers.add_parser('rebuild', help='Reconstruct a directory for a version')
    rebuild_cmd.add_argument('version', help='Version name')
    rebuild_cmd.add_argument('root', help='Root Directory')
    rebuild_cmd.set_defaults(func=cmd_rebuild)
    
    diff_cmd = subparsers.add_parser('diff', help='Build diffs from a version')
    diff_cmd.add_argument('base', help='Base version name')
    diff_cmd.add_argument('output', help='Output')
    diff_cmd.set_defaults(func=cmd_diff)

    args = parser.parse_args()
    if not hasattr(args, 'func'):
        parser.print_help()
    else:
        args.func(args)

if __name__ == '__main__':
    main()
