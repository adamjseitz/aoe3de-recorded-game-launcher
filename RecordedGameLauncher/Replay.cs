﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.IO.Compression;
using System.Net.Http.Headers;

namespace RecordedGameLauncher
{
    [Serializable()]
    public class ReplayException : System.Exception {
        public ReplayException() : base() { }
        public ReplayException(string message) : base(message) { }
        public ReplayException(string message, System.Exception inner) : base(message, inner) { }
        protected ReplayException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }

    public class Replay
    {
        string filePath;
        string version = null;
        string executable = null;

        public Replay(string filePath)
        {
            this.filePath = filePath;
        }

        public string GetVersion()
        {
            if (version == null)
            {
                ReadVersion();
            }
            return version;
        }

        public string GetExecutable()
        {
            if (executable == null)
            {
                ReadVersion();
            }
            return executable;
        }
        
        public bool IsDefinitiveEdition()
        {
            if (executable == null)
            {
                ReadVersion();
            }
            return executable.ToLower().Equals("aoe3de_s.exe");
        }

        public string GetPrettyGameName()
        {
            if (executable == null)
            {
                ReadVersion();
            }
            switch(executable.ToLower())
            {
                case "aoe3de_s.exe":
                    return "Age of Empires 3: Definitive Edition";
                case "age3.exe":
                    return "Age of Empires 3";
                case "age3x.exe":
                    return "Age of Empires 3: The WarChiefs";
                case "age3y.exe":
                    return "Age of Empires 3: The Asian Dynasties";
                case "age3f.exe":
                    return "Age of Empires 3: The Asian Dynasties - ESOC Patch";
            }
            return null;
        }

        private void ReadVersion()
        {
            using (FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                uint recFormatVersion = 0;

                fileStream.Position = 10L;
                using (DeflateStream deflateStream = new DeflateStream(fileStream, CompressionMode.Decompress))
                using (BinaryReader reader = new BinaryReader(deflateStream))
                {
                    recFormatVersion = reader.ReadUInt32();

                    if (recFormatVersion == 3) {
                        reader.ReadBytes(0x10b - 4);
                    } else if (recFormatVersion == 4) {
                        reader.ReadBytes(0x111 - 4);
                    } else {
                        throw new ReplayException(String.Format("Unsupported recorded game format: {0}", recFormatVersion));
                    }

                    uint strlen = reader.ReadUInt32();
                    version = Encoding.Unicode.GetString(reader.ReadBytes((int)strlen * 2));
                    string[] versionComponents = version.Split(' ');

                    if (versionComponents.Length < 2)
                    {
                        throw new ReplayException(String.Format("Cannot parse version string: {0}", version));
                    }

                    executable = versionComponents[0];
                    version = versionComponents[1];
                }
            }
        }
    }
}
