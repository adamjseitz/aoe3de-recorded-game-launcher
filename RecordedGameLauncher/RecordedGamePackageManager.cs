﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecordedGameLauncher
{
    class RecordedGamePackageManager
    {
        string directoryPath;
        List<RecordedGamePackage> packages = new List<RecordedGamePackage>();

        public RecordedGamePackageManager(string directoryPath)
        {
            this.directoryPath = directoryPath;
            LoadPackages();
        }

        public string GetFilesPath()
        {
            return Path.Combine(directoryPath, "Files");
        }

        public void LoadPackages()
        {
            string[] fileEntries = Directory.GetFiles(directoryPath, "*.json");
            foreach (string fileName in fileEntries)
            {
                packages.Add(new RecordedGamePackage(this, Path.Combine(directoryPath, fileName)));
            }
        }

        public RecordedGamePackage GetPackage(string recid)
        {
            foreach (RecordedGamePackage package in packages)
            {
                if (package.MatchesRecId(recid))
                {
                    return package;
                }
            }
            return null;
        }
    }
}
