﻿using System;
using System.IO;
using System.Linq;
using System.Security;
using System.Text.Json.Serialization;

namespace RecordedGameLauncher
{
    [Serializable()]
    public class ConfigurationException : System.Exception
    {
        public ConfigurationException() : base() { }
        public ConfigurationException(string message) : base(message) { }
        public ConfigurationException(string message, System.Exception inner) : base(message, inner) { }
        protected ConfigurationException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }

    class ConfigurationData
    {
        public string GamePath { get; set; }
        public string GameDocumentsPath { get; set; }
        public bool FirstRun { get; set; }
    }

    class Configuration
    {
        string dirPath;
        string confPath;
        ConfigurationData data;

        public Configuration()
        {
            InitDirectory();
            Load();
        }

        private void InitDirectory() {
            dirPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "AoE3DEReplayLauncher");
            Console.WriteLine("Config: " + dirPath);
            confPath = Path.Combine(dirPath, "config.json");
            Directory.CreateDirectory(dirPath);
        }
        private void Load()
        {
            string jsonText;
            try
            {
                jsonText = File.ReadAllText(confPath);
            }
            catch (FileNotFoundException)
            {
                LoadDefaults();
                return;
            }
            catch (Exception ex) when (
                ex is PathTooLongException ||
                ex is DirectoryNotFoundException ||
                ex is IOException ||
                ex is UnauthorizedAccessException ||
                ex is SecurityException ||
                ex is NotSupportedException
            ) {
                throw new ConfigurationException("Failed to read configuration file: " + ex.Message);
            }

            try
            {
                data = System.Text.Json.JsonSerializer.Deserialize<ConfigurationData>(jsonText);
            }
            catch (System.Text.Json.JsonException ex) {
                throw new ConfigurationException("Failed to parse configuration JSON:" + ex.Message);
            }
        }
        private void LoadDefaults()
        {
            data = new ConfigurationData();
            data.GamePath = "C:\\Program Files (x86)\\Steam\\steamapps\\common\\AoE3DE";
            data.GameDocumentsPath = FindDocumentsDir();
            data.FirstRun = true;
        }

        private string FindDocumentsDir()
        {
            string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), "Games\\Age of Empires 3 DE");
            try
            {
                string[] subdirs = Directory.GetDirectories(path);

                foreach (string subdir in subdirs)
                {
                    string subdir_basename = Path.GetFileName(subdir.TrimEnd(Path.DirectorySeparatorChar));
                    if (subdir_basename.All(Char.IsNumber))
                    {
                        return subdir;
                    }
                }
            }
            catch (Exception ex) when (
                ex is PathTooLongException ||
                ex is DirectoryNotFoundException ||
                ex is IOException ||
                ex is UnauthorizedAccessException
            )
            {
                return null;
            }
            return null;
        }
        public void Save()
        {
            data.FirstRun = false;
            File.WriteAllText(confPath, System.Text.Json.JsonSerializer.Serialize<ConfigurationData>(data));
        }
        public string GetGamePath()
        {
            return data.GamePath;
        }

        public void SetGamePath(string path)
        {
            data.GamePath = path;
        }
        public string GetGameDocumentsPath()
        {
            return data.GameDocumentsPath;
        }

        public void SetGameDocumentsPath(string path)
        {
            data.GameDocumentsPath = path;
        }

        public bool IsFirstRun()
        {
            return data.FirstRun;
        }
    }
}
