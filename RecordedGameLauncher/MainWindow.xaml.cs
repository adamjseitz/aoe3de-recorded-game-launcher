﻿using System;
using System.Windows;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Threading;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Text;

namespace RecordedGameLauncher
{

    public class LaunchProgressData
    {
        public string Message { get; set; }
        public string Status { get; set; }
        public float Progress { get; set; }
        public bool Determinate { get; set; }

        public LaunchProgressData(bool d, float p, string status, string m)
        {
            Message = m;
            Status = status;
            Progress = p;
            Determinate = d;
        }
    }


    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        RecordedGamePackageManager packageManager;
        Configuration config;
        RecordedGamePackage stagedPackage = null;
        bool isPackageApplied = false;
        string recordedGamePath;
        private MD5 hashAlgo = MD5.Create();

        public MainWindow()
        {
            InitializeComponent();
            base.Closing += this.MainWindow_Closing;

            config = new Configuration();
            packageManager = new RecordedGamePackageManager(Path.GetFullPath(".\\Cache"));

            if (config.IsFirstRun())
            {
                DoFirstRun();
            }
        }

        private static string BytesToHexString(byte[] bytes)
        {
            StringBuilder result = new StringBuilder(bytes.Length * 2);

            for (int i = 0; i < bytes.Length; i++)
                result.Append(bytes[i].ToString("x2"));

            return result.ToString();
        }

        private string HashFile(string filepath)
        {
            using (FileStream stream = File.OpenRead(filepath))
            {
                return BytesToHexString(hashAlgo.ComputeHash(stream));
            }
        }

        private void BrowseForRecordedGame(object sender, RoutedEventArgs ev)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                Multiselect = false,
                DefaultExt = ".age3Yrec",
                Filter = "Recorded Game Files (*.age3Yrec)|*.age3Yrec",
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Recent),
            };

            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                textFileName.Text = openFileDialog.FileName;
                textFileName.Visibility = Visibility.Visible;

                Replay rec = new Replay(openFileDialog.FileName);
                recordedGamePath = openFileDialog.FileName;
                textGame.Text = rec.GetPrettyGameName();

                stagedPackage = packageManager.GetPackage(rec.GetVersion());
                if (stagedPackage == null)
                {
                    textVersion.Text = "Unknown";
                    textVersion.Visibility = Visibility.Visible;
                    System.Windows.Forms.MessageBox.Show("Unrecognized version " + rec.GetVersion(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                textVersion.Text = stagedPackage.GetVersionString();
                textVersion.Visibility = Visibility.Visible;
            }

            // TODO: Clear fields in the else case?
        }

        private async void Launch(object sender, RoutedEventArgs ev)
        {
            if (recordedGamePath == null)
            {
                return;
            }

            // First, copy recorded game into the Savegame dir if needed
            if (NormalizePath(config.GetGameDocumentsPath()) != NormalizePath(Path.GetDirectoryName(recordedGamePath)))
            {
                bool doCopy = true;
                string targetPath = Path.Combine(config.GetGameDocumentsPath(), "Savegame", Path.GetFileName(recordedGamePath));
                if (File.Exists(targetPath))
                {
                    if (HashFile(targetPath) == HashFile(recordedGamePath))
                    {
                        doCopy = false;
                    }
                    else
                    {
                        string message = String.Format("The recorded game \"{0}\" already exists, would you like to replace it?", targetPath);
                        var result = System.Windows.Forms.MessageBox.Show(message, "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                        if (result == System.Windows.Forms.DialogResult.No)
                        {
                            return;
                        } else
                        {
                            File.Delete(targetPath);
                        }
                    }
                }

                if (doCopy)
                {
                    File.Copy(recordedGamePath, targetPath);
                }
            }

            string exeHash = HashFile(Path.Combine(config.GetGamePath(), "AoE3DE_s.exe"));
            Console.WriteLine("Executable hash: " + exeHash);
            if (exeHash != "c7404b9c07fd20ba8e2f5d1bed15cbb2")
            {
                System.Windows.Forms.MessageBox.Show("The Replay Launcher was built for a different version of Age of Empires 3: Definitive Edition.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            // Start out indeterminate until we start completing files
            isPackageApplied = true;
            progressBarLaunch.IsIndeterminate = true;
            textProgress.Text = "Initializing...";
            panelMain.Visibility = Visibility.Collapsed;
            panelLaunching.Visibility = Visibility.Visible;

            var pfunc = new Progress<LaunchProgressData>((data) => {
                if (data.Message != null)
                {
                    Console.Write(data.Message);
                    listProgressEntries.Items.Add(data.Message);
                    listProgressEntries.ScrollIntoView(listProgressEntries.Items.GetItemAt(listProgressEntries.Items.Count - 1));
                }

                if (data.Status != null)
                {
                    Console.WriteLine(data.Status);
                    textProgress.Text = data.Status;
                    listProgressEntries.Items.Add(data.Status);
                    listProgressEntries.ScrollIntoView(listProgressEntries.Items.GetItemAt(listProgressEntries.Items.Count - 1));
                }

                progressBarLaunch.IsIndeterminate = !data.Determinate;
                if (data.Determinate)
                {
                    progressBarLaunch.Value = (int)Math.Round(data.Progress * 100);
                }
            });

            await Task.Run(() => LaunchWorkerThread(pfunc));

            isPackageApplied = false;
            panelMain.Visibility = Visibility.Visible;
            panelLaunching.Visibility = Visibility.Collapsed;
        }
        private static string NormalizePath(string path)
        {
            return Path.GetFullPath(new Uri(path).LocalPath)
                       .TrimEnd(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar)
                       .ToUpperInvariant();
        }

        private void LaunchWorkerThread(IProgress<LaunchProgressData> progress)
        {
            progress.Report(new LaunchProgressData(false, 0, "Applying compatibility package...", null));
            stagedPackage.Apply(config.GetGamePath(), progress);

            progress.Report(new LaunchProgressData(false, 0, "Launching game...", null));
            using (Process proc = new Process())
            {
                proc.StartInfo.UseShellExecute = false;
                proc.StartInfo.FileName = Path.Combine(config.GetGamePath(), "AoE3DE_s.exe");
                proc.Start();
                proc.WaitForExit();
            }

            progress.Report(new LaunchProgressData(false, 0, "Awaiting main window...", null));
            Stopwatch st = new Stopwatch();
            st.Start();

            // The executable talks to steam, exits, and then steam launches it again, 
            // so we can't just wait for that process to exit
            Process gameProcess = null;
            while (gameProcess == null)
            {
                Process[] processlist = Process.GetProcessesByName("AoE3DE_s");
                // Wait until the AoE3:DE window exists
                foreach (Process process in processlist)
                {
                    if (!String.IsNullOrEmpty(process.MainWindowTitle))
                    {
                        gameProcess = process;
                        break;
                    }
                }

                // Timeout after 5 minutes
                if (st.Elapsed.TotalSeconds > 300)
                {
                    System.Windows.Forms.MessageBox.Show("Did not find game process", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            st.Stop();

            progress.Report(new LaunchProgressData(false, 0, "Awaiting game exit...", null));
            gameProcess.WaitForExit();

            progress.Report(new LaunchProgressData(false, 0, "Restoring game files...", null));
            stagedPackage.Remove(config.GetGamePath(), progress);
        }

        private void DoFirstRun()
        {
            textFirstRunMessage.Visibility = Visibility.Collapsed;
            textboxPath.Text = config.GetGamePath();
            textboxDocsPath.Text = config.GetGameDocumentsPath();
            panelMain.Visibility = Visibility.Collapsed;
            panelFirstRun.Visibility = Visibility.Visible;
        }

        private void FirstRunNext(object sender, RoutedEventArgs ev)
        {
            // TODO: validate path contains the game
            if (!Directory.Exists(textboxPath.Text))
            {
                textFirstRunMessage.Text = "Selected installation directory does not exist";
                textFirstRunMessage.Visibility = Visibility.Visible;
                return;
            }
            else if (!File.Exists(Path.Combine(textboxPath.Text, "AoE3DE_s.exe")))
            {
                textFirstRunMessage.Text = "Selected installation directory does not contain AoE3DE_s.exe";
                textFirstRunMessage.Visibility = Visibility.Visible;
                return;
            }
            else if (!Directory.Exists(textboxDocsPath.Text))
            {
                textFirstRunMessage.Text = "Selected documents directory does not exist";
                textFirstRunMessage.Visibility = Visibility.Visible;
                return;
            }
            else if (!Directory.Exists(Path.Combine(textboxDocsPath.Text, "Savegame")))
            {
                textFirstRunMessage.Text = "Selected documents directory does not contain a \"Savegame\" folder";
                textFirstRunMessage.Visibility = Visibility.Visible;
                return;
            }
            else
            {
                textFirstRunMessage.Visibility = Visibility.Collapsed;
            }

            config.SetGamePath(textboxPath.Text);
            config.SetGameDocumentsPath(textboxDocsPath.Text);
            config.Save();

            panelFirstRun.Visibility = Visibility.Collapsed;
            panelMain.Visibility = Visibility.Visible;
        }

        private void FirstRunBrowse(object sender, RoutedEventArgs ev)
        {
            FolderBrowserDialog openFolderDialog = new FolderBrowserDialog
            {
                Description = "Select the Age of Empires 3: Definitive Edition installation directory",
                ShowNewFolderButton = false,
            };

            if (Directory.Exists(textboxPath.Text))
            {
                openFolderDialog.SelectedPath = textboxPath.Text;
            }
            else
            {
                openFolderDialog.SelectedPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86);
            }

            if (openFolderDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                textFirstRunMessage.Visibility = Visibility.Collapsed;
                textboxPath.Text = openFolderDialog.SelectedPath;
            }
        }

        private void FirstRunDocsBrowse(object sender, RoutedEventArgs ev)
        {
            FolderBrowserDialog openFolderDialog = new FolderBrowserDialog
            {
                Description = "Select the Age of Empires 3: Definitive Edition documents directory",
                ShowNewFolderButton = false,
            };

            if (Directory.Exists(textboxPath.Text))
            {
                openFolderDialog.SelectedPath = textboxDocsPath.Text;
            }
            else
            {
                openFolderDialog.SelectedPath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            }

            if (openFolderDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                textFirstRunMessage.Visibility = Visibility.Collapsed;
                textboxDocsPath.Text = openFolderDialog.SelectedPath;
            }
        }

        void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (isPackageApplied)
            {
                const string message = "Closing now may corrupt your installation of Age of Empires 3: Definitive Edition. Are you sure you want to close?";
                const string caption = "Close Warning";
                var result = System.Windows.Forms.MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                e.Cancel = result == System.Windows.Forms.DialogResult.No;
            }
        }
    }
}
