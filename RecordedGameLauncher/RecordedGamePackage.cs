﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json.Serialization;
using System.Threading;

namespace RecordedGameLauncher
{
    class RecordedGamePackageFile
    {
        [JsonPropertyName("h")]
        public string hash { get; set; }
        [JsonPropertyName("p")]
        public string path { get; set; }
    }

    class RecordedGamePackageData
    {
        public string recid { get; set; }
        [JsonPropertyName("base")]
        public string base_version { get; set; }
        [JsonPropertyName("target")]
        public string target_version { get; set; }
        public IList<RecordedGamePackageFile> files { get; set; }
        public IList<RecordedGamePackageFile> restore_files { get; set; }
    }

    class RecordedGamePackage
    {
        private RecordedGamePackageManager manager;
        private string path;
        private RecordedGamePackageData packageData;

        public RecordedGamePackage(RecordedGamePackageManager manager, string path)
        {
            this.path = path;
            this.manager = manager;

            packageData = System.Text.Json.JsonSerializer.Deserialize<RecordedGamePackageData>(File.ReadAllText(path));
        }

        public void Remove(string game_root, IProgress<LaunchProgressData> progress)
        {
            string cache_root = manager.GetFilesPath();

            int i = 0;
            int num_items = packageData.files.Count + packageData.restore_files.Count;
            foreach (RecordedGamePackageFile file in packageData.files)
            {
                string path = Path.Combine(game_root, file.path);
                string msg = String.Format("Deleting {0}...", path);
                progress.Report(new LaunchProgressData(true, i / (float)num_items, null, msg));
                File.Delete(path);

                // Delete empty directories (recursing up when necessary)
                string dir = Path.GetDirectoryName(path);
                DirectoryInfo dirInfo = new DirectoryInfo(dir);
                while (true)
                {
                    if (Directory.GetFiles(dir).Length + Directory.GetDirectories(dir).Length == 0)
                    {
                        msg = String.Format("Deleting {0}...", dir);
                        progress.Report(new LaunchProgressData(true, i / (float)num_items, null, msg));
                        Directory.Delete(dir);
                    }
                    else 
                    {
                        break;
                    }
                    dirInfo = dirInfo.Parent;
                    dir = dirInfo.FullName;
                }
                i++;
            }

            foreach (RecordedGamePackageFile file in packageData.restore_files)
            {
                string src = Path.Combine(cache_root, file.path);
                string dst = Path.Combine(game_root, file.path);
                string msg = String.Format("Restoring {0}...", dst);
                progress.Report(new LaunchProgressData(true, i / (float)num_items, null, msg));
                Directory.CreateDirectory(Path.GetDirectoryName(dst));
                File.Copy(src, dst);
                i++;
            }
            progress.Report(new LaunchProgressData(true, 1.0f, null, null));
        }

        public void Apply(string game_root, IProgress<LaunchProgressData> progress)
        {
            string cache_root = manager.GetFilesPath();

            int i = 0;
            foreach (RecordedGamePackageFile file in packageData.files)
            {
                string src = Path.Combine(cache_root, file.hash);
                string dst = Path.Combine(game_root, file.path);

                string msg = String.Format("Creating {0}...", dst);
                progress.Report(new LaunchProgressData(true, i / (float)packageData.files.Count, null, msg));
                Directory.CreateDirectory(Path.GetDirectoryName(dst));
                File.Copy(src, dst);
                i++;
            }
            progress.Report(new LaunchProgressData(true, 1.0f, null, null));
        }
        public bool MatchesRecId(string recid)
        {
            return packageData.recid.Equals(recid);
        }

        public string GetVersionString()
        {
            return packageData.target_version;
        }
    }
}
